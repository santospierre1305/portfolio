import Head from "next/head";

const App = () => {
  return (
    <div>
      <Head>
        <title>Santos Pierre</title>
      </Head>
      <div className="bg-gray-100 h-screen flex items-center">
        <div className="mx-auto max-w-screen-xl">
          <div className="text-center">
            <h2 className="tracking-tight leading-10 font-extrabold text-gray-900 sm:leading-none lg:text-6xl md:text-5xl text-4xl">
              Work In <span className="text-indigo-600"> Progress ...</span>
            </h2>
            <div className="mt-5 md:mt-8 lg:flex lg:flex-row lg:justify-center sm:items-center lg:space-y-0 lg:space-x-5 sm:flex-col space-x-3 space-y-0">
              <a href="https://github.com/santos-pierre/" className="md:w-4/5 sm:w-full">
                <div className="flex rounded-md shadow justify-center items-center px-4 py-4 bg-indigo-600 hover:bg-indigo-400 space-x-5">
                  <p className="text-lg text-white font-semibold">
                    <span className="inline-block align-top mb-1">My Github</span>
                  </p>
                  <svg className="h-8 w-8" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitHub icon</title><path d="M12 .297c-6.63 0-12 5.373-12 12 0 5.303 3.438 9.8 8.205 11.385.6.113.82-.258.82-.577 0-.285-.01-1.04-.015-2.04-3.338.724-4.042-1.61-4.042-1.61C4.422 18.07 3.633 17.7 3.633 17.7c-1.087-.744.084-.729.084-.729 1.205.084 1.838 1.236 1.838 1.236 1.07 1.835 2.809 1.305 3.495.998.108-.776.417-1.305.76-1.605-2.665-.3-5.466-1.332-5.466-5.93 0-1.31.465-2.38 1.235-3.22-.135-.303-.54-1.523.105-3.176 0 0 1.005-.322 3.3 1.23.96-.267 1.98-.399 3-.405 1.02.006 2.04.138 3 .405 2.28-1.552 3.285-1.23 3.285-1.23.645 1.653.24 2.873.12 3.176.765.84 1.23 1.91 1.23 3.22 0 4.61-2.805 5.625-5.475 5.92.42.36.81 1.096.81 2.22 0 1.606-.015 2.896-.015 3.286 0 .315.21.69.825.57C20.565 22.092 24 17.592 24 12.297c0-6.627-5.373-12-12-12"/></svg>
                </div>
              </a>
              <a href="https://games.santospierre.com" className="md:w-4/5 sm:w-full">
                <div className="flex rounded-md shadow justify-center items-center px-4 py-4 bg-red-600 hover:bg-red-400 space-x-5">
                  <p className="text-lg text-white font-semibold">
                    <span className="inline-block align-top mb-1">Game Aggregator</span>
                  </p>
                  <svg className="h-8 w-8 text-white" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z"></path><path d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div> 
  )
}

export default App;